package org.vijai.test.steps;

import static io.restassured.RestAssured.given;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class StepDefinitions {
  private String host;
  private String endpoint;
  private String endpointURL;
  private Response response;

  @Given("^This is test$")
  public void this_is_test() {
    System.out.println("This is test step");
  }

  @Given("^host is (.*?)$")
  public void host_is_https_reqres_in(String hostVal) {
    host = hostVal;
  }

  @Given("^endpoint is (.*?)$")
  public void endpoint_is_api_users(String endpointVal) {
    endpoint = endpointVal;
  }

  @When("^HTTP Method GET$")
  public void http_Method_GET() {
    endpointURL = host + endpoint;
    response = given().when().get(endpointURL);
    ResponseBody body = response.getBody();
    String responseBodyAsString = body.asString();
    System.out.println("The response is: \n" + responseBodyAsString);
  }

  @Then("^HTTP status code should be (\\d+)$")
  public void httpStatusCodeShouldBe(int statusCode) {
    response.then().assertThat().statusCode(statusCode);
  }
}
