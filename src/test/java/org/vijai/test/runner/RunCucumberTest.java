package org.vijai.test.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {"src/test/resources/features"},
    glue = {"org.vijai.test.steps"},
    plugin = {
      "html:target/cucumber-html-reports",
      "json:target/reports/cucumber/json/cucumber.json"
    },
    monochrome = true)
public class RunCucumberTest {}
