Feature: Get the single user detail
  Scenario: Test Step
    Given This is test

  Scenario: Validate getting single user detail by ID
    Given host is https://reqres.in
    And endpoint is /api/users/2
    When HTTP Method GET
    Then HTTP status code should be 200
